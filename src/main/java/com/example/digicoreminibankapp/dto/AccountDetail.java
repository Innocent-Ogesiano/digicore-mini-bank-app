package com.example.digicoreminibankapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDetail {
    private String accountName;
    private String accountNumber;
    private double balance;
}
