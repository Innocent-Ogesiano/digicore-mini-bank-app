package com.example.digicoreminibankapp.services;

import com.example.digicoreminibankapp.dto.AccountDetail;
import com.example.digicoreminibankapp.dto.NewAccountDto;
import com.example.digicoreminibankapp.dto.DepositDto;
import com.example.digicoreminibankapp.models.AccountStatement;
import com.example.digicoreminibankapp.models.UserAccount;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserServices {
    boolean createNewAccount(NewAccountDto accountDto);
    boolean authenticateUser(String accountNo, String password) throws Exception;
    boolean deposit (DepositDto depositDto);
    boolean withdraw (String accountNo, double amount);
    List<AccountStatement> getAccountStatement(String accountNo);
    AccountDetail getAccountDetail(String accountNo);
    List<UserAccount> getAllAccounts();
}
