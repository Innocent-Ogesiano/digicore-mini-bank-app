package com.example.digicoreminibankapp.component;

import com.example.digicoreminibankapp.models.UserAccount;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Getter
@Component
public class CustomDataBase {
    Map<String, UserAccount> accountDatabase;

    public boolean saveNewAccount(UserAccount userAccount) {
        if (accountDatabase == null) {
            accountDatabase = new HashMap<>();
        } else {
            for (Map.Entry<String, UserAccount> userAccountEntry : accountDatabase.entrySet()) {
                if (userAccountEntry.getValue().getAccountName().equalsIgnoreCase(userAccount.getAccountName())) {
                    return false;
                }
            }
        }
        accountDatabase.put(userAccount.getAccountNo(), userAccount);
        return true;
    }

    public UserAccount getAccountByAccountNo(String accountNo) {
        if (accountDatabase.containsKey(accountNo)) {
            return accountDatabase.get(accountNo);
        }
        return null;
    }

    public List<UserAccount> getAllAccounts() {
        return new ArrayList<>(accountDatabase.values());
    }

}
