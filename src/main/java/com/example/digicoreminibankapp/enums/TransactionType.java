package com.example.digicoreminibankapp.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
