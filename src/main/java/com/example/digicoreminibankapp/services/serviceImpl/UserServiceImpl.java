package com.example.digicoreminibankapp.services.serviceImpl;

import com.example.digicoreminibankapp.component.CustomDataBase;
import com.example.digicoreminibankapp.dto.AccountDetail;
import com.example.digicoreminibankapp.dto.NewAccountDto;
import com.example.digicoreminibankapp.dto.DepositDto;
import com.example.digicoreminibankapp.enums.TransactionType;
import com.example.digicoreminibankapp.models.UserAccount;
import com.example.digicoreminibankapp.models.AccountStatement;
import com.example.digicoreminibankapp.services.UserServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserServices {
    private final CustomDataBase customDataBase;

    @Autowired
    public UserServiceImpl(CustomDataBase customDataBase) {
        this.customDataBase = customDataBase;
    }

    @Override
    public boolean createNewAccount(NewAccountDto accountDto) {
        UserAccount newAccount = new UserAccount();
        newAccount.setAccountNo(generateAccountNo());
        newAccount.setAccountName(accountDto.getAccountName());
        newAccount.setAccountBalance(accountDto.getInitialDeposit());
        newAccount.setAccountPassword(accountDto.getAccountPassword());
        newAccount.setEnabled(true);
        log.info(newAccount.getAccountNo());

        return customDataBase.saveNewAccount(newAccount);
    }

    private String generateAccountNo() {
        StringBuilder accountNo = new StringBuilder(10);
        for (int i = 0; i < 10; i++) {
            String alphaNumericString = "0123456789";
            int index = (int) (alphaNumericString.length() * Math.random());
            accountNo.append(alphaNumericString.charAt(index));
        }
        return accountNo.toString();
    }

    private UserAccount getAccount (String accountNo) {
        return customDataBase.getAccountByAccountNo(accountNo);
    }

    @Override
    public boolean authenticateUser(String accountNo, String password) throws Exception {
        UserAccount userAccount = getAccount(accountNo);
        if (userAccount == null) {
            throw new Exception("User not found");
        }
        return userAccount.getAccountPassword().equals(password);
    }

    @Override
    public boolean deposit(DepositDto depositDto) {
        UserAccount userAccount = getAccount(depositDto.getAccountNumber());
        if (customDataBase.getAccountDatabase().containsKey(depositDto.getAccountNumber())) {
            userAccount.setAccountBalance(userAccount.getAccountBalance() + depositDto.getAmount());
            customDataBase.getAccountDatabase()
                    .get(depositDto.getAccountNumber())
                    .setAccountBalance(
                            userAccount.getAccountBalance()
                    );
            List<AccountStatement> accountStatements;
            if (userAccount.getAccountStatements() == null) {
                accountStatements = new ArrayList<>();
            } else {
                accountStatements = userAccount.getAccountStatements();
            }
            AccountStatement statement = new AccountStatement();
            statement.setTransactionDate(Date.valueOf(LocalDate.now()));
            statement.setTransactionType(TransactionType.DEPOSIT);
            statement.setTransactionAmount(depositDto.getAmount());
            statement.setCurrentAccountBalance(userAccount.getAccountBalance());
            statement.setNarration("Cash deposit");
            accountStatements.add(statement);
            customDataBase.getAccountDatabase()
                    .get(depositDto.getAccountNumber())
                    .setAccountStatements(accountStatements);
            return true;
        }
        return false;
    }

    @Override
    public boolean withdraw(String accountNo, double amount) {
        UserAccount userAccount = getAccount(accountNo);
        if (userAccount.getAccountBalance() - amount < 500) {
            return false;
        } else {
            userAccount.setAccountBalance(userAccount.getAccountBalance() - amount);
            List<AccountStatement> accountStatements;
            if (userAccount.getAccountStatements() == null) {
                accountStatements = new ArrayList<>();
            } else {
                accountStatements = userAccount.getAccountStatements();
            }
            AccountStatement statement = new AccountStatement();
            statement.setTransactionDate(Date.valueOf(LocalDate.now()));
            statement.setTransactionType(TransactionType.WITHDRAWAL);
            statement.setTransactionAmount(amount);
            statement.setCurrentAccountBalance(userAccount.getAccountBalance());
            statement.setNarration("Cash withdrawal");
            accountStatements.add(statement);
            customDataBase.getAccountDatabase()
                    .get(accountNo)
                    .setAccountStatements(accountStatements);
            customDataBase.getAccountDatabase()
                    .get(accountNo)
                    .setAccountBalance(userAccount.getAccountBalance());
            return true;
        }

    }

    @Override
    public List<AccountStatement> getAccountStatement(String accountNo) {
        UserAccount userAccount = getAccount(accountNo);
        return userAccount.getAccountStatements();
    }

    @Override
    public AccountDetail getAccountDetail(String accountNo) {
        AccountDetail accountDetail = new AccountDetail();
        UserAccount userAccount = getAccount(accountNo);
        log.info(userAccount.getAccountNo());
        accountDetail.setAccountNumber(userAccount.getAccountNo());
        accountDetail.setAccountName(userAccount.getAccountName());
        accountDetail.setBalance(userAccount.getAccountBalance());
        return accountDetail;
    }

    @Override
    public List<UserAccount> getAllAccounts() {
        return customDataBase.getAllAccounts();
    }
}
