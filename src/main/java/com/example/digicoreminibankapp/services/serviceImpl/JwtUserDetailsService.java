package com.example.digicoreminibankapp.services.serviceImpl;

import com.example.digicoreminibankapp.component.CustomDataBase;
import com.example.digicoreminibankapp.dto.MyUserDetails;
import com.example.digicoreminibankapp.models.UserAccount;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements  UserDetailsService {
    @Autowired
    private CustomDataBase dataBase;
    @Autowired
    private PasswordEncoder bcryptEncoder;
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String accountNo) throws UsernameNotFoundException {
        UserAccount userAccount = dataBase.getAccountByAccountNo(accountNo);
        if (userAccount == null) {
            throw new Exception("User not found");
        }
        return new MyUserDetails(userAccount);
//          Set<UserAccount> accountDetails = userService.getAllAccount();
//          if (accountDetails.size() != 0) {
//              UserAccount userAccount = new UserAccount();
//              for (UserAccount accountDetail : accountDetails) {
//                  if (accountDetail.getAccountNo().equals(accountNo)) {
//                      userAccount = accountDetail;
//                  }
//              }
//              if (userAccount == null) {
//                  throw new Exception("User not found");
//              }
//              return new MyUserDetails(userAccount);
//          }
//          User user = accountDetail.orElseThrow(() ->
//                  new UsernameNotFoundException("No user found with email : " + accountNo));
//
//        if(user.getIsEnabled()){
//            return new MyUserDetails(user);
//        }
//        throw new AccountNotEnabledException("Account is disabled");
//        return null;
    }
}
