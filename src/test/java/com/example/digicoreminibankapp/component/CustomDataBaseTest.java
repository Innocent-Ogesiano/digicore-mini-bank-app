package com.example.digicoreminibankapp.component;

import com.example.digicoreminibankapp.DigicoreMiniBankAppApplication;
import com.example.digicoreminibankapp.models.UserAccount;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = DigicoreMiniBankAppApplication.class)
class CustomDataBaseTest {
    @Autowired
    CustomDataBase customDataBase;

    @Test
    void saveNewAccount() {
        UserAccount account1 = new UserAccount("1234567890","Innocent",
                "1234",500,true, null);
        UserAccount account2 = new UserAccount("1234509876","Innocent",
                "1234",500,true, null);
        UserAccount account3 = new UserAccount("0987654321","Jane",
                "1234",500,true, null);
        boolean status = customDataBase.saveNewAccount(account1);
        assertTrue(status);
        status = customDataBase.saveNewAccount(account2);
        assertFalse(status);
        status = customDataBase.saveNewAccount(account3);
        assertTrue(status);
        assertEquals(2, customDataBase.getAccountDatabase().size());
        assertTrue(customDataBase.getAccountDatabase().containsKey(account1.getAccountNo()));
    }

    @Test
    void getAccountByAccountNo() {
        UserAccount account1 = new UserAccount("1234567890","Innocent",
                "1234",500,true, null);
        UserAccount account3 = new UserAccount("0987654321","Jane",
                "1234",500,true, null);
        boolean status = customDataBase.saveNewAccount(account1);
//        assertTrue(status);
        status = customDataBase.saveNewAccount(account3);
//        assertTrue(status);
        assertEquals(2, customDataBase.getAccountDatabase().size());
        assertTrue(customDataBase.getAccountDatabase().containsKey(account1.getAccountNo()));
        UserAccount returnedUser = customDataBase.getAccountByAccountNo(account1.getAccountNo());
        assertEquals(returnedUser, account1);
    }

    @Test
    void getAllAccounts() {
        UserAccount account1 = new UserAccount("1234567890","Innocent",
                "1234",500,true, null);
        UserAccount account2 = new UserAccount("1234509876","Innocent",
                "1234",500,true, null);
        UserAccount account3 = new UserAccount("0987654321","Jane",
                "1234",500,true, null);
        boolean status = customDataBase.saveNewAccount(account1);
//        assertTrue(status);
        status = customDataBase.saveNewAccount(account2);
//        assertFalse(status);
        status = customDataBase.saveNewAccount(account3);
//        assertTrue(status);
        assertEquals(2, customDataBase.getAccountDatabase().size());
        assertTrue(customDataBase.getAccountDatabase().containsKey(account1.getAccountNo()));
        List<UserAccount> users = customDataBase.getAllAccounts();
        assertEquals(2, users.size());
        assertTrue(users.contains(account3));
    }
}