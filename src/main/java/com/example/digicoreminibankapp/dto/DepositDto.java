package com.example.digicoreminibankapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepositDto {
    private String accountNumber;
    private double amount;
}
