package com.example.digicoreminibankapp.services.serviceImpl;

import com.example.digicoreminibankapp.component.CustomDataBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    CustomDataBase customDataBase;
    @InjectMocks
    UserServiceImpl userService;

    @Test
    void createNewAccount() {
    }

    @Test
    void authenticateUser() {
    }

    @Test
    void deposit() {
    }

    @Test
    void withdraw() {
    }

    @Test
    void getAccountStatement() {
    }

    @Test
    void getAccountDetail() {
    }
}