package com.example.digicoreminibankapp.controllers;

import com.example.digicoreminibankapp.configurations.JwtTokenUtil;
import com.example.digicoreminibankapp.dto.*;
import com.example.digicoreminibankapp.models.AccountStatement;
import com.example.digicoreminibankapp.models.UserAccount;
import com.example.digicoreminibankapp.services.UserServices;
import com.example.digicoreminibankapp.services.serviceImpl.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserServices userServices;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;

    @Autowired
    public UserController(UserServices userServices, AuthenticationManager authenticationManager,
                          JwtTokenUtil jwtTokenUtil, JwtUserDetailsService userDetailsService) {
        this.userServices = userServices;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/create_account")
    public ResponseEntity<String> createNewAccount (@RequestBody NewAccountDto accountDto) {
        if (accountDto.getInitialDeposit() < 500) {
            return new ResponseEntity<>("Invalid amount", HttpStatus.BAD_REQUEST);
        }
        boolean status = userServices.createNewAccount(accountDto);
        if (status) {
            return new ResponseEntity<>("Account created", HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Account name already exist", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

        boolean status = userServices.authenticateUser(jwtRequest.getAccountNo(), jwtRequest.getPassword());
        if (status) {
            final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getAccountNo());

            final String jwtToken = jwtTokenUtil.generateToken(userDetails);

            return ResponseEntity.ok(new JwtResponse(jwtToken));
        }
        return new ResponseEntity<>("User not found", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/deposit")
    public ResponseEntity<String> deposit (@RequestBody DepositDto depositDto) {
        if (depositDto.getAmount() > 1000000 || depositDto.getAmount() < 1) {
            return new ResponseEntity<>("Invalid amount", HttpStatus.BAD_REQUEST);
        }
        boolean status = userServices.deposit(depositDto);
        if (status) {
            return new ResponseEntity<>("Money deposited successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Account does not exist", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/withdraw")
    public ResponseEntity<String> withdrawFromAccount(@RequestParam double amount, HttpServletRequest request) throws Exception {
        if (amount < 1) {
            return new ResponseEntity<>("Invalid amount", HttpStatus.BAD_REQUEST);
        }
        String token = request.getHeader("Authorization").substring(7);
        String accountNo = jwtTokenUtil.getAccountNoFromToken(token);
        if (accountNo == null) {
            return new ResponseEntity<>("Account does not exist", HttpStatus.BAD_REQUEST);
        }
            boolean status = userServices.withdraw(accountNo, amount);
            if (status) {
                return new ResponseEntity<>("Withdrawal successful", HttpStatus.OK);
            }
            return new ResponseEntity<>("Insufficient balance", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/account_statement")
    public ResponseEntity<?> getAccountStatement(HttpServletRequest request) {
        String token = request.getHeader("Authorization").substring(7);
        String accountNo = jwtTokenUtil.getAccountNoFromToken(token);
        if (accountNo == null) {
            return new ResponseEntity<>("Account does not exist", HttpStatus.BAD_REQUEST);
        }
        List<AccountStatement> accountStatements = userServices.getAccountStatement(accountNo);
        return new ResponseEntity<>(accountStatements, HttpStatus.OK);
    }

    @GetMapping("/account_info")
    public ResponseEntity<?> getAccountInfo(HttpServletRequest request) {
        String token = request.getHeader("Authorization").substring(7);
        String accountNo = jwtTokenUtil.getAccountNoFromToken(token);
        if (accountNo == null) {
            return new ResponseEntity<>("Account does not exist", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userServices.getAccountDetail(accountNo), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserAccount>> getAllAccounts () {
        return new ResponseEntity<>(userServices.getAllAccounts(), HttpStatus.OK);
    }
}
