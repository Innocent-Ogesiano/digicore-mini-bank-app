package com.example.digicoreminibankapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreMiniBankAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigicoreMiniBankAppApplication.class, args);
    }

}
