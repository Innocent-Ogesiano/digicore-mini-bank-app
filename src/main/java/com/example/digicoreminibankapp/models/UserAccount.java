package com.example.digicoreminibankapp.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserAccount {
    private String accountNo;
    private String accountName;
    private String accountPassword;
    private double accountBalance;
    private boolean isEnabled;
    private List<AccountStatement> accountStatements;
}
