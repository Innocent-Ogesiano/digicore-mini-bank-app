package com.example.digicoreminibankapp.models;

import com.example.digicoreminibankapp.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountStatement {
    private Date transactionDate;
    private String narration;
    private TransactionType transactionType;
    private double transactionAmount;
    private double currentAccountBalance;
}
