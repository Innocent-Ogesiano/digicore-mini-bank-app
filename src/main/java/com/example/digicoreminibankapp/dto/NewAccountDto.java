package com.example.digicoreminibankapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewAccountDto {
    private String accountName;
    private String accountPassword;
    private double initialDeposit;
}
